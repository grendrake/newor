\chapter{Types of Entities}

Every entity in the world is broadly divided into one of three categories:
\spcword{gods}, \spcword{demons}, and \spcword{mortals}. Demons and mortals are
typically broken down into further subcategories, but gods are fairly uniform
in their capabilities.

Note that, while biological, plants are largely excluded from this
description. Should it matter, plants are similar to mortals, but possess only
a species and not a type. Plants do not exhibit intentional, volitional
behaviour and this is considered the main defining characteristic of a plant.

\section{Immortality}

There are a lot of entities in Newor that are either very long lived or
entirely immortal. While the details of this are different for every type of or
individual entity, such immortality is typically divided classified into a few
categories.

\spcword{True immortals} are beings that already existed or were created when
the world was created and that will still exist or will be destroyed when the
world ends. The true immortal category is always tentative; there is no reliable
way to tell the difference between a true immortal and an entity that just
happens to have survived for the same length of time outside of the latter
eventually dying. Although often referred to as such, true immortals are not
technically considered to be alive. \spcword{Gods} and \spcword{demonlords} are
the most prominent true immortals.

An entity is said to have \spcword{stolen immortality} if it originally lacked
immortality but has gained it in some way such that they will still exist at
the end of the world. Like with true immortals, this is a tentative
classification as it can only be proven by not dying before the world ends. The
principle example of stolen immortality is the \spcword{undying}.

Many demons and advanced mages possess \spcword{false immortality}. This is
when an entity extends its life beyond its natural span through the use of
magic. Although typically long lived and not subject to the ravages of aging,
those with false immortality have no protection against death.

Finally, a few entities simple posses a \spcword{long lifespan}. These entities
are not considered to be immortal and will still age and die, it will just take
them longer.

\section{Worship}

It is possible for a mortal to dedicate themselves to a god or to a powerful demon.
This is what is meant by \spcword{worshipping} or \spcword{following} that entity.
Such worship provides benefits to both parties.
A mortal may worship only a single entity, but entities desiring worship can easily have hundreds of thousands of worshippers.
A worship relationship may only be established by mutual consent and it is abolished as soon as either party wants to be released.

The worshipped entity is capable of perceiving everything that its worshippers can perceive.
For gods, this is the only way they are able to perceive the world.
The entity is also capable of directly and securely communicating with individual worshippers or arbitrary groups of them.
Entities capable of being worshipped possess a uniquely extreme ability to multitask.
The typical god can communicate with hundreds of thousands of followers simultaneously or follow independent chains of thought.
Worshippable demons, even demonlords, can rarely multitask above tens of thousands of simultaneous actions.

A worshipper is able to directly and securely\footnote{Though if they speak out loud their words can be overheard in the normal fashion.} communicate with the object of their worship (this is often referred to as prayer).
This does not mean that the god is paying attention, only that the message was passed along, so to speak.
The main benefit of worship, however, is that worshippers may be granted use of a fraction of the god's power.
The god cannot control how the worshipper uses such power, but may end the worship relationship if the mortal's action are too out of line of the god's interests.

Power granted this way takes the form of fixed abilities that superficially resemble spells.
Unlike spells, however, they function as though cast by the god using primal magic and thus come from a reserve of nearly infinite energy.
Most divine spells have a very high complexity, though it is unclear whether this is deliberate or just the result of gods not needing to worry about casting difficulty.

While such divines spell do not drain the mortal caster's energy, said caster is still not able to use them endlessly.
The amount of divine magic that a mortal can channel is determined by their \spcword{attunement} to the object of their worship.
Attunement is determined by how closely the mortals actions align with the god's interests.
Very few people truly have no attunement to a particular god, but developing greater attunement requires deliberate action.
In principle, an individual with 100\% attunement would be able to cast divine magic with ease, but few are capable of coming even close to that level of attunement.
The minds of gods are demons are sufficiently alien to those of mortals that few mortals can achieve even a superficial form of comprehension.


\section{Gods}\label{sec:entities:gods}

\epigraph{Do not look to the gods to improve yourself; they do not know
  change. What they are, they shall ever be.}{Sklar the Elder}

Gods are non-corporeal, non-spatial entities that primarily interact with the
world through followers who have dedicated themselves to the god's service. All
gods are without gender or physical sex, though they may be given a gender by
their followers. Most gods are indifferent to this, though it sometimes causes
confusion when different followers decide on gendering the same god
differently.

Being non-corporeal and non-spatial means that gods do not possess any physical
form nor do they exist in any specific location. Because of this, they are not
capable of innately interacting with or perceiving the world. They can only
perceive the world through the eyes of those who have dedicated themselves to
the god and they can act only by granting power to their followers. They cannot
force a follower to perform (or not perform) an action, though a god displeased
with their follower's actions can revoke any granted powers. Gods may
communicate with their followers (typically in the form of a voice in the
follower's head) and followers may communicate with their god without having to
speak aloud. Outside of this, gods cannot directly effect their followers.

Magic from the gods is always primal magic. Because any spells are technically
cast by the god, rather than the follower, this remains safe for followers to
make use of. Magical traces from godly magic can always be traced by to the
specific god it originated from, but often not the follower who caused it to be
cast.

Gods are considered to be \spcword{true immortals}. Every god has existed since
the world was created and cannot be destroyed in any way. New gods cannot be
created, nor are existing gods capable of significant change.

It is possible for gods to be contained or restrained. The simpler method is to
eliminate all of the gods followers. Without followers, they are unable to act
on or even perceive the world. The other way is more esoteric and known only to
a very few. This is a situation that the gods are highly in favour of and
something they are unified in continuing.


\section{Demons}\label{sec:entities:demons}

Demons are entities that are capable of innately using primal magic and that
are not gods. When using magic, demons draw energy from the world rather than
from themselves. Not all demon are capable of drawing as much energy as quickly
as other demons though, and their ability to draw power is the primary
determination of how powerful the demon is. The secondary determination is
based on the demon's intelligence; while demons are typically considered to be
highly intelligent, this is often an illusion based on the experience they've
gained from having lived immensely long lives. A more intelligent demon is able
to construct primal magic spells more quickly and that use less energy. Some
particularly low-intelligence demons may be unable to construct more
complicated spells at all.

While demons often take on the appearance of a mortal species, possibly with
extra features, they are never truly members of those species. Most demons are
fully capable of changing their forms at will. They can also take on the
appearance and capabilities of either sex at will, though many demons will
spend most of their time as a specific sex.

An \spcword{outer demon} is one that originates from outside the world. An
\spcword{elder demon} is one that predates the world. By definition, all elder
demons are also outer demons and it is typically not considered necessary to
specify this.

Demons are often divided into a number of subtypes. To a degree these
categories are arbitrary and simply based on the presence (or absence) of
particular features. It is entirely possible for a demon to belong to multiple
categories or to straddle the border between different categories.

\subsection{Demonlords}

The \spcword{demonlords} are the most powerful variety of demons. In terms of
raw power, they are approximately equals to the gods. Unlike gods, demonlords
have a physical body and allows them to directly observe and interact with the
world. This gives them a significant advantage over the gods as they are not
reliant on having followers to carry out their wishes while still being capable
of taking just as much advantage from followers as gods can. Due to having a
physical body, most demonlords take on a gender identity.

All known demonlords are \spcword{true immortals} and were sealed away by the
end of the \spcword{War of the Demon's Fall}. This does not mean they are no
longer active; while some, such as the \spcword{Sleeping Kings}, are locked in
slumber, others are fully aware and continue to interact with the world through
their followers. The most notable active demonlord is \spcword{Viantra} who is
sealed beneath the capital city of the draks.

\subsection{Greater Demons}

Greater demons are immensely powerful beings capable of amazing feats that
surpass anything all but the greatest of mortals can do. Like gods and
demonlords, greater demons are capable of having dedicated followers and even
sharing their power with them. Unlike gods and demonlords, sharing their power
this way effectively reduces the power available to the demon. As a result,
most greater demons are far more selective about their followers.

\subsection{Lesser Demons}

Lesser demons are the weaker variety of demon; they typically have capabilities
roughly equivalent to a mid to high ranked wizard, but specific individuals may
fall outside this range on both sides.

\subsection{Spirit Demons}

Spirit demons are demons lacking a corporeal form. They appear in the world
like ghosts. Sometimes spirit demons can be difficult to verify the type of as
they are capable of constructing a body out of inanimate material and operating
it in a manner similar to an automaton.

\subsection{Technical Demons}

Sometimes entities are discovered that are labelled demons only because no
other category fits. These are the technical demons. Most are outer demons and
many are elder as well. It is not uncommon for them to fit the description of
greater demons as well, though that label is normally only applied if the demon
regularly interacts with the world.

Many technical demons struggle to understand the basic mechanisms on which the
world works or otherwise struggle to interact with the regular world in a
meaningful way. Zarkis is known to struggle to tell the difference between
``alive'' and ``dead'' as long as both ghost and corpse are present.

Technical demons are sometimes instead categorized as \spcword{things} by those
who dislike indecipherable lumps in their taxonomic systems. \spcword{Abbarax}
and \spcword{Zarkis} are technical demons.

\subsection{Reproduction}

Unlike gods, it is possible for demons both to reproduce and to die. More
powerful demons tend to be difficult to kill, however, and typically have
little desire to deal with offspring. Children and death are both far more
common among lesser demons than any other category.

Unlike mortals, demons cannot reproduce with their own kind. In order for a
demon to have a child, they most pair with a mortal. While many lesser demons
are just are fertile as mortals, as a demon's power rises, they become less
fertile.

Not all children of demons are themselves demons. The likelihood of the child
being a demon depends on a number of factors. The first is the power level of
the demon; powerful demons are less likely to have demon children. The second
is whether the demon acts as the mother or father; child of demons who act as
the mother are more likely to be demons. On rare occasion, the child of a demon
and a mortal will instead be a half-demon. These are considered mortals and
described on page \pageref{sec:entities:halfdemon}.

While children who are demons will not have a type (as they aren't mortals),
those who are not born as a demon will have the type and species of their
non-demonic parent. Demon children will appear to have the species of their
non-demon parent.

The power of a demon child will typically be similar to that of their demon
parent, but typically varies up or down some. Demons are not born with their
full power. It develops slowly, only reaching its adult capacity once the demon
reaches physical maturity.


\section{Mortals}

Mortals are entities that are composed of two separate parts, a \spcword{body}
and a \spcword{soul}. As a result, unlike gods or demons, types of mortals are
determined in two ways: by \spcword{species} and by \spcword{type}. Species
determines a mortals body, including their appearance and capabilities, and
their type determines their cognitive capabilities.

While mortals are capable of using primal magic, they do not possess the innate
understanding of it that demons or gods do. Actually using primal magic will
cause damage to both mortal bodies and minds.

\subsection{Species}

Numerous species are known. They are divided into \spcword{groups} and
\spcword{subgroups}. These are distinguished by the present or absence of
certain, specific features. (That is, they're the equivalent of taxonomy for a
world with cross-species reproduction).

A species provides an \spcword{appearance}. While many traits of a species exist
as a range or with a variety of options, the range of available traits is
defined by an individual's species. These variable traits are often inherited
from, or are a blend of one's parents, though occasionally species will possess
traits that are non-inheritable; the values of these are effectively randomized
at conception.

One's appearance can be changed beyond these limits through the use of magic
without changing an individual's species, however. Many capabilities are
defined by one's appearance without being specifically linked a species. Wings,
for instance, typically grant the ability to fly regardless of whether the
species normal possesses flight or wings.

An individual's species also defines their \spcword{instincts}. These are
impulses or urges that come not from rational thought, but from one's
underlying biology. It is not uncommon for individuals of higher types to deny
the influence, or even existence, of instincts even as they engage in the
behaviour those instincts encourage.

Reproductive capacity and cycle is also determined by species. The most obvious
of these is how long and when an individual is fertile or possessed of sexual
desire. Other traits, such as a species' litter size or number of physical
sexes are also determined this way.

\subsection{Type}

Every mortal has a \spcword{type}. While an individual's type is independent of
their species, most species have a \spcword{standard type}---this is simply the
most common type members of that species possess. A mortal's type is fixed for
the duration of their life; it cannot be changed, even with the use of magic.
Type can be easily determined using magic of nearly any variety.

Types are rated from low to high with higher types being considered more
intelligent and capable. The list below is sorted from lowest to highest.

\subsubsection{Animal}

Animals are non-sapient creatures whose behaviour is almost entirely driven
by instinct. They possess enough variation for individuals to have
different personalities from each other. Animals are never capable of speech,
even if they belong to a species that normally can speak.

\subsubsection{Person}

A \spcword{person} is the type of a typical sapient individual. While people
still possess and feel the urges from instincts, they are equally ruled by
their mind and rationality (which is not to say they always make good or
rational decisions).

\subsubsection{Half-Demon}\label{sec:entities:halfdemon}

Half-demons are a possible, but rare, result of a mortal/demon
pairing. Although often considered by mortals to be lucky, half-demons often
find the limitations of their type greater than the benefits. Although a
half-demon is technically of their mortal parent's species, most take their
appearance from how their demonic parent looked when they were conceived, even
if this appearance is significantly different from what their species would
indicate.

Once they reach physical maturity, half-demons do not age and show a resistance
to both magic and physical harm. As mages, they are consistently capable of
handling more energy for spells and often possess an aptitude for magic, though
they lack of innate understanding of primal magic that a true demon
possesses. Despite this, they are unable to use or be effected by rites. When it
comes to summoning magic, they count as demons rather than mortals.

Half-demons are completely sterile and are incapable of reproduction in any
form. In addition, their souls are not stable or reincarnated like other types
of mortals; when they die, their soul immediately dissipates and is
gone. Similarly, things that effect the soul (such as becoming one of the
\spcword{People of the Choice}) have no effect on them.

\subsection{Spirits and Death}

When a mortal dies, their soul separates from their body. This disembodied soul
retains all the cognitive function the mortal possessed in life, though their
personality may seem to shift as they are no longer subject to their body's
instincts.

While primal magic or summoning can be used to put a disembodied soul into a
living body this typically entails problems beyond that inherit to the magic
itself. Not the least is that having multiple souls in the same body is
disorienting and control of the body passes between all resident souls at random.

Most souls will linger in the world for several months at least, this time
being determined by how much they want to stay and how connected they feel to
the world. After this time, the soul will fade and be reincarnated.

If the mortal had dedicated themselves to a god or greater demon, their soul is
instantly pulled to that entity to be rewarded and punished as that entity
desires. This continues until the god or demon releases the soul to be
reincarnated.

Most mortal souls have been reincarnated multiple times. It is sometimes
possible to remember memories from a past life, though these are always
fragmented and possess missing details. Occasionally, in unusual circumstances
of conception, a new soul is created. These new souls cannot (for obvious
reasons) access memories from a past life.

\subsection{Reproduction}

Unlike the real world, whether conception has occurred is determined
immediately after sperm enters the womb; each batch of sperm has only a single
chance to cause a pregnancy.

The fertility of a pairing is determined by the stage of the female's fertility
cycle (i.e. whether they're capable of conceiving at that moment), the species
of the parents (with different species having a harder time conceiving and
differences in group and subgroup reducing this further), and the population
density of the immediate area (with conception being harder in more populated
areas). In addition, magic is capable of both increasing and decreasing the
conception chance up to the point of causing complete sterility or guaranteeing
conception.

The species of a child is determined at the moment of conception. Same species
parents will always produce children of the same species while parents of
different species will normally produce children of the mother's species. If
the father is of a species that is rare in the local area, there is an
increasing chance that the child will be of the father's species instead (in
cases were the father's species is unique, or nearly so, in the area, the
chances of offspring being the father's species are nearly certain). The
species (and sex) of a child can be changed using magic.

A child's type is normally that of their mother.
This can be changed using rites \pr{sec:magic:rites} or with primal magic \pr{sec:magic:primal}.
In addition, some species or individuals are cursed to be \spcword{weakly typed}; such individuals cannot pass on their type to their children unless the other parent is also weakly typed.
Conversely, it is also possible to be \spcword{strongly typed}; children of a strongly typed individual or species will always share that parents type unless the other parent is also strongly typed.


\section{Vermin}

Perhaps the largest group of living things on Newor are the vermin.
Vermin are primarily small insects and other similar pests along with a handful of unusually animate plants.
Despite the normal connotation of the terms, rats and other small scavengers are not included in this category.

The defining characteristics of vermin are their generally small size and lack of intelligence.
Vermin act exclusively according to their instincts and are often compared to automatons in terms of predictability.
They are typically tough to kill and breed quickly as well as possessing a surprising degree of magic resistance.
As a result, they are often a focus of research in academic circles, but this only increases the hatred the majority of the population has for them.

While vermin can breed rapidly with their own kind, it is impossible for non-vermin to reproduce with vermin.
